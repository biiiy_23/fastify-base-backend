const customMessage = (...args) => {
    if (args.length == 0) {
        return {
            status: 500,
            message: 'Server is busy'
        }
    } else if(args.length == 1) {
        return {
            status: 500,
            message: args[0]
        }
    } else {
        if (args[1] instanceof String || typeof args[1] === 'string')
            return {
                status: args[0],
                message: args[1]
            }
        else
            return {
                status: args[0],
                data: args[1]
            }
    }
}

module.exports = {
    customMessage: customMessage,
}